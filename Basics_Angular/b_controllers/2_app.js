// register angular module, used with directive ng-app
// parameter 1: Name , parameter 2: Dependencies
angular.module('app',[]);

// register angular controller, used directly by the ng-controller
// parameter 1: Controller name , parameter 2: Implementation

angular.module('app').controller('MainCtrl',function($scope){

// we'll need to inject $scope and add some data to it
$scope.message = 'hello';

$scope.change = function (msg) {
  $scope.message = msg;
};

});
