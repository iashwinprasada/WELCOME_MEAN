# AngularJS

this repo is a walk-through for the main tutorial at [Thinkster](https://thinkster.io/a-better-way-to-learn-angularjs).

+ All the files are ready to be run, just open and get started!

+ Added many lines to emphasize on certain concepts.
+ Follow the comments,the flow is kept simple.

+ Use external links that are inserted.

+ Folders have been named in an order
  + Please follow the order a(1,2,3..) , b(1,2,3..)

Prerequisites

    Moderate knowledge of HTML, CSS, and JavaScript
    Basic Model-View-Controller (MVC) concepts
    The Document Object Model (DOM)
    JavaScript functions, events, and error handling

-------
<i>Ashwin Prasad
