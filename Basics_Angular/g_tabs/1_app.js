;(function(window) {

angular.module('app', [])
.directive('tab', function() {
  return {
    // restrict: 'E' which which means the directive will be an element
    restrict: 'E',
    transclude: true,
    // template to specify an HTML template string that will be inserted into the DOM
    template: '<div role="tabpanel" ng-show="active" ng-transclude></div>',
    // '^' character instructs the directive to move up the scope hierarchy one level and look for the controller on tabset
    require: '^tabset',
    scope: {
        // The '@' symbol is a special symbol in Angular that means this scope property should be a string.
        heading: '@'
    },
    // this function is similar to an angular controller function and is home to the majority of the directive's logic
    link: function(scope, elem, attr, tabsetCtrl) {
        scope.active = false
        tabsetCtrl.addTab(scope)
    }
  }
})

.directive('tabset', function() {
  return {
    restrict: 'E',
    transclude: true,
    scope: { },
    // rather than defining a template string, we're using templateUrl to specify an external template.
    templateUrl: '1_tabset.html',
    bindToController: true,
    controllerAs: 'tabset',
    controller: function() {
      var self = this
      self.tabs = []

      self.addTab = function addTab(tab) {
        self.tabs.push(tab)

        if(self.tabs.length === 1) {
            tab.active = true
        }
      }
      // ng-click directive that invokes a select() method      self.select = function(selectedTab) {
        angular.forEach(self.tabs, function(tab) {
          if(tab.active && tab !== selectedTab) {
            tab.active = false;
          }
        })
        selectedTab.active = true;
      }
    }
  }
})

})(window);
